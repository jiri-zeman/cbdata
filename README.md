# Entrance test of applicant Jiří Zeman for cbData
This project is a simple web application that displays a list of Star Wars planets and their details.
It is built using [Next.js](https://nextjs.org/) and [TypeScript](https://www.typescriptlang.org/).
The application is deployed on [Vercel](https://vercel.com/) and can be accessed [here](https://zeman-cbdata.vercel.app/).
This is a project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started
- The project uses [node.js](https://nodejs.org/) 20.x or newer
- In the project root, run the command to install the packages
```bash
npm run i
```
- For local development, run the command
```bash
npm run dev
```
- If server running. Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.


