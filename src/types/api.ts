export type Planet = {
  name: string;
  rotation_period: string;
  orbital_period: string;
  diameter: string;
  climate: string;
  gravity: string;
  terrain: string;
  surface_water: string;
  population: string;
  residents: string[];
  films: string[];
  created: string;
  edited: string;
  url: string;
};

export type Planets = Planet[];

export type NoPlanetsData = {
  detail: string;
};

export type PlanetsData = {
  count: number;
  next: null | string;
  previous: null | string;
  results: Planets;
};

export type PlanetResponse = Planet | NoPlanetsData;

export type PlanetsDataResponse = PlanetsData | NoPlanetsData;
