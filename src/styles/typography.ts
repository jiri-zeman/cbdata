import { css } from 'styled-components';
import { colors } from '@/styles/variables';

export const typography = css`
  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    font-weight: 500;
    line-height: 1.25;
    margin-block-end: 1rem;
    margin-block-start: 0;
  }

  h1 {
    font-size: 3.6rem;
  }

  h2 {
    font-size: 3.2rem;
  }

  h3 {
    font-size: 2.8rem;
  }

  p,
  ul,
  ol,
  dl {
    margin-block-end: 1rem;
    margin-block-start: 0;
  }

  a {
    color: ${colors.primary};
    text-decoration: underline;

    &:hover,
    &:focus {
      color: ${colors.primary};
      text-decoration: none;
    }
  }
`;
