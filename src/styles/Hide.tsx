'use client';

import styled from 'styled-components';

export const Hide = styled.span.attrs({
  'aria-hidden': true,
})`
  display: none;
`;
