export const colors = {
  white: '#fff',
  black: '#000',
  primary: '#987d2a',
  darkGray: '#0d0d0d',
  gray: '#1D1E1F',
};

export const borderRadius = {
  small: '0.8rem',
};
