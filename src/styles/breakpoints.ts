const size = {
  xs: '20em', // 320px
  sm: '36em', // 576px
  md: '48em', // 768px
  lg: '62em', // 992px
  xl: '75em', // 1200px
  xxl: '100em', // 1600px
};

export const device = {
  xs: `(min-width: ${size.xs})`,
  sm: `(min-width: ${size.sm})`,
  md: `(min-width: ${size.md})`,
  lg: `(min-width: ${size.lg})`,
  xl: `(min-width: ${size.xl})`,
  xxl: `(min-width: ${size.xxl})`,
};
