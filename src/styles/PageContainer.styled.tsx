'use client';

import styled from 'styled-components';

export const StyledPageContainer = styled.div`
  max-inline-size: 160rem;
  inline-size: 100%;
  margin: 0 auto;

  main {
    padding-inline: 2rem;
    margin-block-start: 5rem;
  }
`;
