'use client';

import styled, { css } from 'styled-components';
import { colors, borderRadius } from '@/styles/variables';

type ButtonProps = {
  $variant?: 'default' | 'primary';
};

export const StyledButton = styled.button<ButtonProps>`
    border-radius: ${borderRadius.small};
    border: 0.1rem solid transparent;
    display: inline-block;
    inline-size: fit-content;
    transition: all 0.2s ease-in-out;
    text-align: left;
    text-decoration: none;
    padding: 0.75rem 1.5rem;

    ${({ $variant = 'default' }) => {
      if ($variant === 'default') {
        return css`
          background-color: ${colors.black};
          border-color: ${colors.primary};
          color: ${colors.primary};

          &:focus,
          &:hover {
            background-color: ${colors.primary};
            color: ${colors.white};
          }
        `;
      } else if ($variant === 'primary') {
        return css`
          background-color: ${colors.primary};
          border-color: ${colors.primary};
          color: ${colors.white};

          &:focus,
          &:hover {
            background-color: ${colors.white};
            color: ${colors.primary};
          }
        `;
      }
    }}
}
`;
