'use client';

import { createGlobalStyle } from 'styled-components';
import { normalize } from 'styled-normalize';
import { colors } from '@/styles/variables';
import { typography } from '@/styles/typography';

export const GlobalStyle = createGlobalStyle`
    ${normalize}
    html {
        font-size: 62.5%;
    }

    body {
        background-color: ${colors.black};
        color: ${colors.white};
        background-image: url("/img/backgrounds/background.jpeg");
        background-position: center top;
        background-repeat: repeat;
        direction: ltr;
        font-family: Helvetica, Arial, sans-serif;
        font-size: 1.6rem;
        font-weight: 400;
        line-height: 1.5;
        overflow-x: hidden;
        position: relative;
        writing-mode: horizontal-tb;
    }

    img {
        block-size: auto;
        max-inline-size: 100%;
        vertical-align: middle;
    }

    button {
        border: none;
        background-color: transparent;
        cursor: pointer;
        outline: none;
        padding: 0;
    }

    ${typography}
`;
