'use client';

import styled from 'styled-components';
import { borderRadius, colors } from '@/styles/variables';
import { device } from '@/styles/breakpoints';

export const StyledDataNotFound = styled.section`
  background-color: ${colors.gray};
  border-radius: ${borderRadius.small};
  display: grid;
  gap: 3rem;
  padding: 3rem;

  @media ${device.md} {
    align-items: center;
    grid-template-columns: repeat(2, 1fr);
    gap: 5rem;
    padding: 5rem;
  }

  @media ${device.lg} {
    padding: 9rem;
  }

  h1 {
    margin-block-end: 0;
  }

  p {
    margin-block-end: 0;
    margin-block-start: 2rem;
  }

  button,
  a {
    margin-block-start: 2rem;
  }
`;
