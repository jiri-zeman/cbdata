import type { FC } from 'react';
import Link from 'next/link';
import { StyledButton } from '@/styles/Button.styled';
import defaultImg from '/public/img/characters/baby-yoda.webp';
import Image, { type StaticImageData } from 'next/image';
import { StyledDataNotFound } from '@/components/dataNotFound/DataNotFound.styled';

type NotFoundProps = {
  title: string;
  description: string;
  imgSrc?: string | StaticImageData;
  imgHeight?: number;
  imgWidth?: number;
  linkHref?: string;
  linkText?: string;
  headingAs?: 'h1' | 'h2';
};

export const DataNotFound: FC<NotFoundProps> = ({
  title,
  description,
  imgSrc = defaultImg,
  imgHeight = 675,
  imgWidth = 1200,
  linkHref = '/',
  linkText = 'Go back to home',
  headingAs = 'h2',
}) => {
  const Heading = headingAs;

  return (
    <StyledDataNotFound>
      <Image
        src={imgSrc}
        alt={'millennium-falcon'}
        width={imgWidth}
        height={imgHeight}
      />
      <div>
        <Heading>{title}</Heading>
        <p>{description}</p>
        <Link href={linkHref} passHref legacyBehavior>
          <StyledButton as={'a'}>{linkText}</StyledButton>
        </Link>
      </div>
    </StyledDataNotFound>
  );
};
