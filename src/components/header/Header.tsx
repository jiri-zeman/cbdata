import type { FC } from 'react';
import {
  StyledHeader,
  StyledHeaderLogo,
} from '@/components/header/Header.styled';
import logo from '/public/img/logo/logo.png';
import Image from 'next/image';
import Link from 'next/link';

type HeaderProps = {};

export const Header: FC<HeaderProps> = () => {
  return (
    <StyledHeader>
      <StyledHeaderLogo>
        <Link href={'/'}>
          <Image src={logo} alt={'Star Wars'} width={586} height={254} />
        </Link>
      </StyledHeaderLogo>
    </StyledHeader>
  );
};
