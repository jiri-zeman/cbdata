'use client';

import styled from 'styled-components';

export const StyledHeader = styled.header`
  margin-block-start: 5rem;
  padding-inline: 2rem;
`;

export const StyledHeaderLogo = styled.div`
  margin-inline: auto;
  max-inline-size: 18rem;
`;
