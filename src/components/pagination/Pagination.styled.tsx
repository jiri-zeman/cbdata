'use client';

import { borderRadius, colors } from '@/styles/variables';
import { device } from '@/styles/breakpoints';
import styled from 'styled-components';

export const StyledPagination = styled.nav`
  margin-block-start: 2rem;

  ul {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    list-style-type: none;
    margin-block-end: 0;
    padding-inline-start: 0;
  }
`;

export const StyledPaginationItem = styled.li<{
  $isActiveItem?: boolean;
}>`
  background-color: ${({ $isActiveItem }) => {
    return $isActiveItem ? colors.primary : colors.white;
  }};
  color: ${({ $isActiveItem }) => {
    return $isActiveItem ? colors.white : colors.primary;
  }};

  &:first-child {
    border-start-start-radius: ${borderRadius.small};
    border-end-start-radius: ${borderRadius.small};
  }

  &:last-child {
    border-start-end-radius: ${borderRadius.small};
    border-end-end-radius: ${borderRadius.small};
  }
`;

export const StyledPaginationFakeLink = styled.span`
  display: block;
  padding: 1rem 1.5rem;

  @media ${device.md} {
    padding: 2rem 2.5rem;
  }
`;

export const StyledPaginationLink = styled.a<{ $isDisabled?: boolean }>`
  color: inherit;
  cursor: ${(props) => {
    return props.$isDisabled ? 'not-alowed' : 'pointer';
  }};
  pointer-events: ${(props) => {
    return props.$isDisabled ? 'none' : 'initial';
  }};
  display: block;
  padding: 1rem 1.5rem;
  text-decoration: none;
  transition: color 0.2s ease-in-out;

  &:focus,
  &:hover {
    color: inherit;
    text-decoration: underline;
  }

  @media ${device.md} {
    padding: 2rem 2.5rem;
  }
`;
