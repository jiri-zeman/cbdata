'use client';

import type { FC } from 'react';
import { useSearchParams, usePathname } from 'next/navigation';
import { getHref } from '@/utils/getHref';
import Link from 'next/link';
import { DOTS, usePagination } from '@/hooks/usePagination';
import {
  StyledPagination,
  StyledPaginationFakeLink,
  StyledPaginationItem,
  StyledPaginationLink,
} from '@/components/pagination/Pagination.styled';
import { apiPaginationParameterName } from '@/utils/fetch-API';

export type PaginationProps = {
  totalCount: number;
  pageSize: number;
};

export const Pagination: FC<PaginationProps> = ({ totalCount, pageSize }) => {
  const pathname = usePathname();
  const searchParams = useSearchParams();
  const pageNumberParameterValue = searchParams.get(apiPaginationParameterName);
  const params = Object.fromEntries(searchParams.entries());
  const currentPage = Number(pageNumberParameterValue) || 1;

  const paginationRange = usePagination({ totalCount, pageSize, currentPage });
  if (paginationRange && paginationRange.length < 2) {
    return null;
  }

  const firstPage = paginationRange ? Number(paginationRange.at(0)) : 1;
  const lastPage = paginationRange ? Number(paginationRange.at(-1)) : 1;
  const prevPage = currentPage - 1;
  const nextPage = currentPage + 1;
  const hasPrevPage = prevPage >= firstPage;
  const hasNextPage = nextPage <= lastPage;

  return (
    <StyledPagination>
      <ul>
        <StyledPaginationItem>
          <Link
            passHref
            legacyBehavior
            href={getHref(pathname, {
              ...params,
              [apiPaginationParameterName]: hasPrevPage ? prevPage : firstPage,
            })}
          >
            <StyledPaginationLink $isDisabled={!hasPrevPage}>
              &lt;
            </StyledPaginationLink>
          </Link>
        </StyledPaginationItem>
        {paginationRange &&
          paginationRange.map((pageNumber, index) => {
            if (pageNumber === DOTS) {
              return (
                <StyledPaginationItem key={index}>
                  <StyledPaginationFakeLink>&#8230;</StyledPaginationFakeLink>
                </StyledPaginationItem>
              );
            }

            const isActiveItem = pageNumber === currentPage;

            return (
              <StyledPaginationItem key={index} $isActiveItem={isActiveItem}>
                <Link
                  passHref
                  legacyBehavior
                  href={getHref(pathname, {
                    ...params,
                    [apiPaginationParameterName]:
                      pageNumber === 1 ? firstPage : pageNumber,
                  })}
                >
                  <StyledPaginationLink>{pageNumber}</StyledPaginationLink>
                </Link>
              </StyledPaginationItem>
            );
          })}
        <StyledPaginationItem>
          <Link
            passHref
            legacyBehavior
            href={getHref(pathname, {
              ...params,
              [apiPaginationParameterName]: hasNextPage
                ? nextPage
                : currentPage,
            })}
          >
            <StyledPaginationLink $isDisabled={!hasNextPage}>
              &gt;
            </StyledPaginationLink>
          </Link>
        </StyledPaginationItem>
      </ul>
    </StyledPagination>
  );
};
