'use client';

import styled from 'styled-components';
import { colors } from '@/styles/variables';

export const StyledFooter = styled.footer`
  margin-block: 8rem;
  padding-inline: 2rem;
`;

export const StyledFooterLogo = styled.div`
  position: relative;

  &::before {
    background-color: ${colors.gray};
    block-size: 0.1rem;
    content: '';
    display: block;
    inline-size: 100%;
    inset-block-start: 50%;
    inset-inline-start: 0;
    position: absolute;
    translate: 0 -50%;
    z-index: 1;
  }

  a {
    background-color: ${colors.black};
    display: block;
    margin-inline: auto;
    max-inline-size: 10rem;
    padding-inline: 4rem;
    position: relative;
    z-index: 5;
  }
`;

export const StyledFooterCopyright = styled.div`
  font-size: 1.4rem;
  margin-block-start: 2rem;

  ul {
    display: flex;
    flex-wrap: wrap;
    gap: 2rem;
    list-style-type: none;
    margin-block-end: 0;
    padding-inline-start: 0;

    li:not(:first-of-type) {
      position: relative;

      &::before {
        background-color: ${colors.white};
        block-size: 100%;
        content: '';
        inline-size: 0.1rem;
        inset-inline-start: -1.1rem;
        inset-block-start: 0;
        position: absolute;
      }
    }
  }
`;
