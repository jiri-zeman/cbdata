import type { FC } from 'react';
import {
  StyledFooter,
  StyledFooterCopyright,
  StyledFooterLogo,
} from '@/components/footer/Footer.styled';
import logo from '/public/img/logo/logo.png';
import Link from 'next/link';
import Image from 'next/image';

type FooterProps = {};

export const Footer: FC<FooterProps> = ({}) => {
  return (
    <StyledFooter>
      <StyledFooterLogo>
        <Link href={'/'}>
          <Image src={logo} alt={'Star Wars'} width={586} height={254} />
        </Link>
      </StyledFooterLogo>
      <StyledFooterCopyright>
        <ul>
          <li>© Star Wars, {new Date().getFullYear()}</li>
          <li>
            <Link href={'/planets'}>Planets</Link>
          </li>
        </ul>
      </StyledFooterCopyright>
    </StyledFooter>
  );
};
