import type { FC } from 'react';
import {
  StyledMedia,
  StyledMediaContentContainer,
} from '@/components/media/Media.styled';
import Link from 'next/link';
import { ImageWithFallback } from '@/components/image/ImageWithFallback';
import { StyledButton } from '@/styles/Button.styled';

type MediaProps = {};

export const Media: FC<MediaProps> = ({}) => {
  return (
    <StyledMedia>
      <div>
        <Link href={'/planets'}>
          <ImageWithFallback
            src={'/img/fly-vehicles/x-wing.webp'}
            alt={'webp'}
            width={1024}
            height={705}
          />
        </Link>
        <StyledMediaContentContainer>
          <h2>Planets</h2>
          <p>
            Don&apos;t be afraid and go explore the planets from the world of
            Star Wars.
          </p>
          <Link href={'/planets'} passHref legacyBehavior>
            <StyledButton as={'a'}>Explore planets</StyledButton>
          </Link>
        </StyledMediaContentContainer>
      </div>
    </StyledMedia>
  );
};
