'use client';

import styled from 'styled-components';
import { colors, borderRadius } from '@/styles/variables';

export const StyledMedia = styled.article`
  border-radius: ${borderRadius.small} ${borderRadius.small} 0 0;
  inline-size: 100%;
  max-inline-size: 60rem;
  overflow: hidden;

  div {
    background-color: ${colors.gray};
    margin-block-end: 1.5rem;
    position: relative;

    &::before,
    &::after {
      content: '';
      display: block;
      inset-block-end: -1.4rem;
      position: absolute;
      border-block-start: 1.5rem solid ${colors.gray};
    }

    &::before {
      border-end-start-radius: ${borderRadius.small};
      border-inline-end: 1.5rem solid transparent;
      inset-inline-start: 0;
      inline-size: 45%;
    }

    &::after {
      border-end-end-radius: ${borderRadius.small};
      border-inline-start: 1.5rem solid transparent;
      inset-inline-end: 0;
      inline-size: 20%;
    }
  }
`;

export const StyledMediaContentContainer = styled.div`
  padding: 2.5rem 2rem;

  h2 {
    font-size: 1.8rem;
    margin-block-end: 0;
  }

  p {
    margin-block-start: 1rem;
    font-style: italic;
  }

  button,
  a {
    margin-block-start: 2rem;
  }
`;
