'use client';

import type { FC } from 'react';
import { useState } from 'react';

import Image, { type StaticImageData } from 'next/image';
import type { ImageProps } from 'next/image';
import fallbackImgSrc from '/public/img/placeholders/star-wars.webp';

type ImageWithFallbackProps = ImageProps & {
  fallbackSrc?: string | StaticImageData;
  fallbackHeight?: number;
  fallbackWidth?: number;
};

export const ImageWithFallback: FC<ImageWithFallbackProps> = ({
  fallbackSrc = fallbackImgSrc,
  fallbackHeight,
  fallbackWidth,
  ...props
}) => {
  const [error, setError] = useState(false);
  return (
    <Image
      {...props}
      src={error ? fallbackSrc : props.src}
      width={error ? fallbackWidth ?? 2560 : props.width}
      height={error ? fallbackHeight ?? 1546 : props.width}
      onError={() => {
        return setError(true);
      }}
      alt={props.alt ?? ''}
    />
  );
};
