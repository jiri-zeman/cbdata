'use client';

import type { FC } from 'react';
import { useEffect } from 'react';
import { PlanetsItem } from '@/components/planets/PlanetsItem';
import type {
  NoPlanetsData,
  Planets as PlanetsType,
  PlanetsData,
} from '@/types/api';
import { StyledPlanets } from '@/components/planets/Planets.styled';
import { DataNotFound } from '@/components/dataNotFound/DataNotFound';
import { usePlanetContext } from '@/providers/planet-context-provider';

type PlanetsProps = {
  data: PlanetsData | NoPlanetsData;
};

export const Planets: FC<PlanetsProps> = ({ data }) => {
  const { setPlanetsData } = usePlanetContext();

  let planetsData: PlanetsType = [];
  if (data && 'results' in data) {
    planetsData = data.results;
  } else {
    planetsData = [];
  }

  useEffect(() => {
    setPlanetsData(planetsData);
  }, []);

  if (!planetsData || planetsData.length === 0) {
    return (
      <DataNotFound
        title={'Planets not found'}
        description={'Failed to load data.'}
      />
    );
  }

  return (
    <StyledPlanets>
      {planetsData.map((item, index) => {
        return (
          <li key={index}>
            <PlanetsItem data={item} />
          </li>
        );
      })}
    </StyledPlanets>
  );
};
