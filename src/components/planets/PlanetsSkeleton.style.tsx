'use client';

import styled, { css, keyframes } from 'styled-components';
import { device } from '@/styles/breakpoints';
import { borderRadius, colors } from '@/styles/variables';

const loading = keyframes`
    to {
        background-position: 200% 100%;
    }
`;

const animation = css`
  position: relative;

  &::before {
    content: '';
    display: block;
    block-size: 100%;
    inline-size: 100%;
    position: absolute;
    inset: 0;
    background: linear-gradient(
      90deg,
      rgba(255, 255, 255, 0),
      rgba(81, 82, 82, 0.3),
      rgba(255, 255, 255, 0)
    );
    animation: ${loading} 3s infinite;
    background-size: 200% 100%;
  }
`;

export const StyledPlanetsSkeleton = styled.ul`
  display: grid;
  gap: 2rem;
  list-style-type: none;
  margin: 0;
  padding: 0;

  @media ${device.md} {
    grid-template-columns: repeat(2, 1fr);
  }
`;

export const StyledPlanetsSkeletonItem = styled.li`
  border-radius: ${borderRadius.small} ${borderRadius.small} 0 0;
  block-size: 100%;
  color: ${colors.primary};
  display: flex;
  flex-direction: column;
  inline-size: 100%;
  margin-inline: auto;
  max-inline-size: 100rem;
  overflow: hidden;
`;

export const StyledPlanetsSkeletonArticle = styled.article`
  background-color: ${colors.white};
  block-size: 100%;
  display: flex;
  flex-direction: column;
  gap: 2rem;
  margin-block-end: 1.5rem;
  padding: 2rem;
  position: relative;

  &::before,
  &::after {
    content: '';
    display: block;
    inset-block-end: -1.4rem;
    position: absolute;
    border-block-start: 1.5rem solid ${colors.white};
  }

  &::before {
    border-end-start-radius: ${borderRadius.small};
    border-inline-end: 1.5rem solid transparent;
    inset-inline-start: 0;
    inline-size: 45%;
  }

  &::after {
    border-end-end-radius: ${borderRadius.small};
    border-inline-start: 1.5rem solid transparent;
    inset-inline-end: 0;
    inline-size: 20%;
  }
`;

export const StyledPlanetsSkeletonImg = styled.div`
  background-color: #f3f3f3;
  border-radius: ${borderRadius.small} ${borderRadius.small} 0 0;
  block-size: 20rem;
  inline-size: 100%;
  list-style-type: none;
  margin: 0;
  padding: 0;
  position: relative;

  @media ${device.md} {
    block-size: 28rem;
  }
`;

export const StyledPlanetsSkeletonHeading = styled.div`
  block-size: 7rem;

  ${animation}
`;

export const StyledPlanetsSkeletonContent = styled.div`
  display: grid;
  gap: 2rem;

  ul {
    display: grid;
    column-gap: 2rem;
    list-style-type: none;
    margin: 0;
    padding: 0;
    row-gap: 0.5rem;

    @media ${device.sm} {
      grid-template-columns: repeat(2, 1fr);
    }

    @media ${device.md} {
      grid-template-columns: 1fr;
    }

    @media ${device.lg} {
      grid-template-columns: repeat(2, 1fr);
    }

    li {
      block-size: 2rem;

      ${animation}
    }
  }

  span {
    display: block;
    block-size: 4rem;
    border-radius: ${borderRadius.small};
    inline-size: 7rem;
    overflow: hidden;

    ${animation}
  }
`;
