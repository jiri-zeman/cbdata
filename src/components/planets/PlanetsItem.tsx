import type { Planet } from '@/types/api';
import type { FC } from 'react';
import { ImageWithFallback } from '@/components/image/ImageWithFallback';
import Link from 'next/link';
import { StyledPlanetsItem } from './PlanetsItem.styled';
import { StyledButton } from '@/styles/Button.styled';

type PlanetsItemProps = {
  data: Planet;
  showDetail?: boolean;
};

export const PlanetsItem: FC<PlanetsItemProps> = ({
  data,
  showDetail = true,
}) => {
  const {
    name,
    url,
    diameter,
    population,
    rotation_period,
    orbital_period,
    climate,
    gravity,
    terrain,
    surface_water,
  } = data;
  const imgSrc = name.toLowerCase().replace(' ', '-');
  const href = new URL(url).pathname.replace('/api', '');

  const imageContent = (
    <ImageWithFallback
      src={`/img/planets/${imgSrc}.jpg`}
      alt={'webp'}
      width={1200}
      height={450}
    />
  );

  return (
    <StyledPlanetsItem>
      {href ? <Link href={href}>{imageContent}</Link> : imageContent}
      <h2>{name}</h2>
      <div>
        <ul>
          <li>
            <strong>Diameter:</strong> {diameter}
          </li>
          <li>
            <strong>Population:</strong> {population}
          </li>
          <li>
            <strong>Rotation period:</strong> {rotation_period}
          </li>
          <li>
            <strong>Orbital period:</strong> {orbital_period}
          </li>
          <li>
            <strong>Climate:</strong> {climate}
          </li>
          <li>
            <strong>Gravity:</strong> {gravity}
          </li>
          <li>
            <strong>Terrain:</strong> {terrain}
          </li>
          <li>
            <strong>Surface water:</strong> {surface_water}
          </li>
        </ul>
        {showDetail && href && (
          <Link href={href} passHref legacyBehavior>
            <StyledButton $variant={'primary'} as={'a'}>
              Detail
            </StyledButton>
          </Link>
        )}
      </div>
    </StyledPlanetsItem>
  );
};
