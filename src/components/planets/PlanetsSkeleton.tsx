import type { FC } from 'react';
import {
  StyledPlanetsSkeleton,
  StyledPlanetsSkeletonArticle,
  StyledPlanetsSkeletonContent,
  StyledPlanetsSkeletonHeading,
  StyledPlanetsSkeletonImg,
  StyledPlanetsSkeletonItem,
} from '@/components/planets/PlanetsSkeleton.style';

type PlanetsSkeletonProps = {};

export const PlanetSkeleton: FC<PlanetsSkeletonProps> = ({}) => {
  return (
    <StyledPlanetsSkeleton>
      {Array.from({ length: 10 }).map((_, index) => {
        return (
          <StyledPlanetsSkeletonItem key={index}>
            <StyledPlanetsSkeletonArticle>
              <StyledPlanetsSkeletonImg></StyledPlanetsSkeletonImg>
              <StyledPlanetsSkeletonHeading></StyledPlanetsSkeletonHeading>
              <StyledPlanetsSkeletonContent>
                <ul>
                  {Array.from({ length: 8 }).map((_, index) => {
                    return <li key={index}></li>;
                  })}
                </ul>
                <span></span>
              </StyledPlanetsSkeletonContent>
            </StyledPlanetsSkeletonArticle>
          </StyledPlanetsSkeletonItem>
        );
      })}
    </StyledPlanetsSkeleton>
  );
};
