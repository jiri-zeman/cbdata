'use client';

import styled from 'styled-components';
import { device } from '@/styles/breakpoints';

export const StyledPlanets = styled.ul`
  display: grid;
  gap: 2rem;
  list-style-type: none;
  margin: 0;
  padding: 0;

  @media ${device.md} {
    grid-template-columns: repeat(2, 1fr);
  }
`;
