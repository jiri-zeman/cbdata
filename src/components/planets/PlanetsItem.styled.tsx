'use client';

import styled from 'styled-components';
import { device } from '@/styles/breakpoints';
import { colors, borderRadius } from '@/styles/variables';

export const StyledPlanetsItem = styled.article`
  border-radius: ${borderRadius.small} ${borderRadius.small} 0 0;
  block-size: 100%;
  color: ${colors.primary};
  display: flex;
  flex-direction: column;
  inline-size: 100%;
  margin-inline: auto;
  max-inline-size: 100rem;
  overflow: hidden;

  h2 {
    background-color: ${colors.primary};
    color: ${colors.white};
    margin: 0;
    padding: 1.5rem 2rem;
  }

  div {
    background-color: ${colors.white};
    block-size: 100%;
    display: flex;
    flex-direction: column;
    gap: 2rem;
    margin-block-end: 1.5rem;
    padding: 2rem;
    position: relative;

    &::before,
    &::after {
      content: '';
      display: block;
      inset-block-end: -1.4rem;
      position: absolute;
      border-block-start: 1.5rem solid ${colors.white};
    }

    &::before {
      border-end-start-radius: ${borderRadius.small};
      border-inline-end: 1.5rem solid transparent;
      inset-inline-start: 0;
      inline-size: 45%;
    }

    &::after {
      border-end-end-radius: ${borderRadius.small};
      border-inline-start: 1.5rem solid transparent;
      inset-inline-end: 0;
      inline-size: 20%;
    }
  }

  ul {
    display: grid;
    column-gap: 2rem;
    list-style-type: none;
    margin: 0;
    padding: 0;
    row-gap: 0.5rem;

    @media ${device.sm} {
      grid-template-columns: repeat(2, 1fr);
    }

    @media ${device.md} {
      grid-template-columns: 1fr;
    }

    @media ${device.lg} {
      grid-template-columns: repeat(2, 1fr);
    }

    li {
      color: ${colors.primary};
      font-weight: 900;

      strong {
        color: ${colors.darkGray};
        font-weight: 500;
      }
    }
  }

  a {
    margin-top: auto;
  }
`;
