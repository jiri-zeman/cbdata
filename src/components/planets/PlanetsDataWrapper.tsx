import type { FC } from 'react';
import { apiPaginationParameterName, fetchAPI } from '@/utils/fetch-API';
import { getHref } from '@/utils/getHref';
import type { PlanetsDataResponse } from '@/types/api';
import { Planets } from '@/components/planets/Planets';

type PlanetsDataWrapperProps = {
  paginationParameterValue: string | string[] | undefined;
};

const getPlanetsData = async (path: string) => {
  return await fetchAPI<PlanetsDataResponse>(path);
};

export const PlanetsDataWrapper: FC<PlanetsDataWrapperProps> = async ({
  paginationParameterValue = '',
}) => {
  const computedPath = getHref('/', {
    [apiPaginationParameterName]: paginationParameterValue,
  });

  const data = await getPlanetsData(computedPath);

  return <Planets data={data} />;
};
