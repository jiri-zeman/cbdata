'use client';

import type { FC } from 'react';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import { apiPaginationParameterName } from '@/utils/fetch-API';
import { getHref } from '@/utils/getHref';
import { StyledResetButton } from '@/components/resetButton/ResetButton.styled';

type ResetButtonProps = {};

export const ResetButton: FC<ResetButtonProps> = ({}) => {
  const { push, refresh } = useRouter();
  const pathname = usePathname();
  const searchParams = useSearchParams();
  const apiPaginationParameterValue = searchParams.get(
    apiPaginationParameterName,
  );
  const params = Object.fromEntries(searchParams.entries());

  if (!apiPaginationParameterValue) {
    return null;
  }

  return (
    <StyledResetButton
      $variant={'primary'}
      onClick={() => {
        if (apiPaginationParameterValue) {
          push(
            getHref(pathname, {
              ...params,
              [apiPaginationParameterName]: undefined,
            }),
          );
          refresh();
        }
      }}
    >
      Reset pagination
    </StyledResetButton>
  );
};
