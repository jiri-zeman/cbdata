'use client';

import styled from 'styled-components';
import { StyledButton } from '@/styles/Button.styled';

export const StyledResetButton = styled(StyledButton)`
  margin-block-end: 2rem;
`;
