import { Media } from '@/components/media/Media';

export default function Home() {
  return (
    <main>
      <Media />
    </main>
  );
}
