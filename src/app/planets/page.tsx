import { apiPaginationParameterName, fetchAPI } from '@/utils/fetch-API';
import { PlanetsDataWrapper } from '@/components/planets/PlanetsDataWrapper';
import { Suspense } from 'react';
import type { SearchParams } from '@/types/type';
import type { Metadata } from 'next';
import { ResetButton } from '@/components/resetButton/ResetButton';
import { Pagination } from '@/components/pagination/Pagination';
import type { PlanetsDataResponse } from '@/types/api';
import { PlanetSkeleton } from '@/components/planets/PlanetsSkeleton';

type HomeProps = {
  searchParams: SearchParams;
};

const getPageData = async (path: string) => {
  const data = await fetchAPI<PlanetsDataResponse>(path);
  if (data && 'count' in data && 'results' in data) {
    return data;
  }

  return undefined;
};

export default async function PlanetsPage({ searchParams }: HomeProps) {
  const data = await getPageData('/');

  return (
    <main>
      <h1>Planets</h1>
      <ResetButton />
      <Suspense
        key={
          searchParams[apiPaginationParameterName]
            ? (searchParams[apiPaginationParameterName] as string)
            : undefined
        }
        fallback={<PlanetSkeleton />}
      >
        <PlanetsDataWrapper
          paginationParameterValue={searchParams[apiPaginationParameterName]}
        />
      </Suspense>
      <Pagination
        totalCount={data?.count ?? 0}
        pageSize={data?.results.length ?? 10}
      />
    </main>
  );
}

export const metadata: Metadata = {
  title: 'Planets',
};
