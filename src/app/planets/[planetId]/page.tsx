import { fetchAPI } from '@/utils/fetch-API';
import type { Metadata } from 'next';
import type { PlanetResponse } from '@/types/api';
import { notFound } from 'next/navigation';
import { PlanetsItem } from '@/components/planets/PlanetsItem';
import { Hide } from '@/styles/Hide';

type PlanetPageProps = {
  params: {
    planetId: string;
  };
};

export default async function PlanetPage({
  params: { planetId },
}: PlanetPageProps) {
  const data = await fetchAPI<PlanetResponse>(`/${planetId}`);
  if (!data || 'detail' in data) {
    notFound();
  }

  return (
    <main>
      <Hide>
        <h1>{data.name}</h1>
      </Hide>
      <PlanetsItem data={data} showDetail={false} />
    </main>
  );
}

export async function generateMetadata({
  params: { planetId },
}: PlanetPageProps): Promise<Metadata> {
  const data = await fetchAPI<PlanetResponse>(`/${planetId}`);

  let title = '';
  if (!data || 'detail' in data) {
    title = `404`;
  } else {
    title = `${data.name}`;
  }

  return {
    title: title,
  };
}
