import millenniumFalcon from '/public/img/fly-vehicles/millennium-falcon.jpg';
import { DataNotFound } from '@/components/dataNotFound/DataNotFound';

export default function NotFound() {
  return (
    <main>
      <DataNotFound
        title={'Planet not found'}
        description={
          'You are looking for an unknown planet, please check if you have entered the correct URL. Alternatively, you can fly back to all the planets and choose the right one there.'
        }
        linkHref={'/planets'}
        linkText={'Go back to all planets'}
        imgSrc={millenniumFalcon}
        imgWidth={875}
        imgHeight={450}
        headingAs={'h1'}
      />
    </main>
  );
}
