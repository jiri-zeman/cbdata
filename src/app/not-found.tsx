import droid from '/public/img/droids/c3-po.jpg';
import { DataNotFound } from '@/components/dataNotFound/DataNotFound';

export default function NotFound() {
  return (
    <main>
      <DataNotFound
        title={'404 - Page not found'}
        description={
          "The page you were looking for was not found. Even C3PO doesn't know what you might be looking for, so please go back to the home page with him and try searching again."
        }
        imgSrc={droid}
        imgWidth={1200}
        imgHeight={800}
        headingAs={'h1'}
      />
    </main>
  );
}
