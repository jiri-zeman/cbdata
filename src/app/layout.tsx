import type { Metadata } from 'next';
import type { PropsWithChildren } from 'react';
import StyledComponentsRegistry from '@/lib/registry';
import { PlanetProvider } from '@/providers/planet-context-provider';
import { GlobalStyle } from '@/styles/GlobalStyle';
import { StyledPageContainer } from '@/styles/PageContainer.styled';
import { Header } from '@/components/header/Header';
import { Footer } from '@/components/footer/Footer';

type RootLayoutProps = Readonly<PropsWithChildren<{}>>;

export default function RootLayout({ children }: RootLayoutProps) {
  return (
    <html lang="en">
      <body>
        <StyledComponentsRegistry>
          <GlobalStyle />
          <StyledPageContainer>
            <PlanetProvider>
              <Header />
              {children}
              <Footer />
            </PlanetProvider>
          </StyledPageContainer>
        </StyledComponentsRegistry>
      </body>
    </html>
  );
}

export const metadata: Metadata = {
  title: {
    template: '%s | Star Wars',
    default: 'Star Wars',
  },
};
