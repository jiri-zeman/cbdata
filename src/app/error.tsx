'use client';

import { useEffect } from 'react';
import unknownDroid from '/public/img/droids/unknown-droid.webp';
import { DataNotFound } from '@/components/dataNotFound/DataNotFound';

type ErrorProps = {
  error: Error & { digest?: string };
};

export default function Error({ error }: ErrorProps) {
  useEffect(() => {
    console.error(error);
  }, [error]);

  return (
    <main className="flex h-full flex-col items-center justify-center">
      <DataNotFound
        title={'Something went wrong'}
        description={`Damn! You are probably trying to do something that the galactic council did not expect, so please send the following error: ${error.message}`}
        imgSrc={unknownDroid}
        imgWidth={1200}
        imgHeight={844}
        headingAs={'h1'}
      />
    </main>
  );
}
