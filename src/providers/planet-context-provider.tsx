'use client';

import type { FC, PropsWithChildren } from 'react';
import { useContext } from 'react';
import { createContext, useState } from 'react';
import type { Planets } from '@/types/api';

type PlanetContextType = {
  planetsData: Planets;
  setPlanetsData: (state: Planets | (() => Planets)) => void;
};

const defaultPlanetDataState: Planets = [];

export const PlanetContext = createContext<PlanetContextType>({
  planetsData: defaultPlanetDataState,
  setPlanetsData: () => {},
});

type PlanetProviderProps = PropsWithChildren<{}>;

export const PlanetProvider: FC<PlanetProviderProps> = ({ children }) => {
  const [providerData, setProviderData] = useState<Planets>(
    defaultPlanetDataState,
  );

  const handleSetProviderData = (state: Planets | (() => Planets)) => {
    setProviderData(state);
  };

  return (
    <PlanetContext.Provider
      value={{
        planetsData: providerData,
        setPlanetsData: handleSetProviderData,
      }}
    >
      {children}
    </PlanetContext.Provider>
  );
};

export const usePlanetContext = () => {
  return useContext(PlanetContext);
};
