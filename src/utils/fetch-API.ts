type FetchOptions = {
  method?: 'GET';
  headers?: Record<string, string>;
  next?: {
    revalidate: number;
  };
};

export const apiPaginationParameterName = 'page';

export const fetchAPI = async <T>(
  path: string = '/',
  options: FetchOptions = {},
): Promise<T> => {
  try {
    const mergedOptions: FetchOptions = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
      next: {
        revalidate: 3600,
      },
      ...options,
    };

    const requestUrl = `https://swapi.dev/api/planets${path}`;
    const response = await fetch(requestUrl, mergedOptions);

    return await response.json();
  } catch (error) {
    console.error(`Fetch API error! Error: ${error}`);
    throw new Error('Fetch API error!');
  }
};
